package main

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	log "gitlab.com/venchoware/go/log4go"
	"net/http"
	"os"
)

type myViper struct {
	*viper.Viper
}

var v myViper

func init() {
	v = myViper{viper.New()}
}

func initArgs(args []string) error {
	pflag.Int("cache_max", 5, "Cache max")
	pflag.String("cache_service", "", "Cache service")
	pflag.String("db_service", "", "Database service")
	pflag.String("port", "", "Listening port")
	pflag.String("loglevel", "INFO", "Log level")

	pflag.CommandLine.Parse(args[1:])
	v.SetEnvPrefix("clf")
	v.AutomaticEnv()
	v.BindPFlags(pflag.CommandLine)

	v.MandatoryCheck("cache_service", "db_service", "port", "cache_max")

	return nil
}

func main() {
	if err := initArgs(os.Args); err != nil {
		log.Fatal("%v", err)
	}

	if err := initLogging(v.GetString("loglevel")); err != nil {
		log.Fatal("%s", err)
	}
	defer log.Close()

	if err := initCache(v.GetString("cache_service"),
		v.GetInt("cache_max")); err != nil {
		log.Fatal("Cannot connect to cache: %v", err)
	}
	defer cacheClose()

	if err := initDB(v.GetString("db_service")); err != nil {
		log.Fatal("Cannot connect to db: %v", err)
	}
	defer closeDB()

	log.Info("Listening on port %s.", v.GetString("port"))
	log.Fatal(http.ListenAndServe(":"+viper.GetString("port"),
		initRoutes()))
}

func initLogging(level string) error {
	if logLevel, err := log.Str2Level(level); err != nil {
		return err
	} else {
		log.NewDefaultLogger(logLevel)
		return nil
	}
}

func (v myViper) MandatoryCheck(args ...string) {
	for _, arg := range args {
		if v.GetString(arg) == "" {
			log.Fatal("Argument %s must be set!", arg)
		}
	}
}
