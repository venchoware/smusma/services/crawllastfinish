package main

import (
	retry "github.com/avast/retry-go"
	"github.com/mediocregopher/radix"
)

var cache *radix.Pool

func initCache(service string, max int) error {
	err := retry.Do(
		func() error {
			var err error
			cache, err = radix.NewPool("tcp", service, max)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)

	return err
}

func cacheClose() error {
	return cache.Close()
}

func cacheCrawlLastFinish(vendor string) (string, error) {
	var finish string
	if err := cache.Do(radix.Cmd(&finish, "GET", vendor)); err != nil {
		return "", err
	}

	return finish, nil
}

func fillCrawlLastFinish(vendor string, lastFinish string) error {
	return cache.Do(radix.Cmd(nil, "SET", lastFinish))
}
