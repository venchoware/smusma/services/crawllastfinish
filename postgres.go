package main

import (
	"context"
	retry "github.com/avast/retry-go"
	"github.com/jackc/pgx"
)

var ctxt context.Context
var db *pgx.Conn

func initDB(svc string) error {
	ctxt = context.Background()
	err := retry.Do(
		func() error {
			var err error
			db, err = pgx.Connect(ctxt, svc)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)

	return err
}

func closeDB() error {
	return db.Close(ctxt)
}

func dbCrawlLastFinish(vendor string) (string, error) {
	var lastFinish string
	if err := db.QueryRow(ctxt,
		"select max(finish) from crawls where vendor = $1",
		vendor).Scan(&lastFinish); err != nil {
		return "", err
	} else {
		return lastFinish, nil
	}
}
