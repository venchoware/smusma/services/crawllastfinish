package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	log "gitlab.com/venchoware/go/log4go"
	"net/http"
)

func initRoutes() *httprouter.Router {
	router := httprouter.New()

	router.GET("/v1/getcrawllastfinish/:vendor", getCrawlLastFinish)

	return router
}

func getCrawlLastFinish(
	w http.ResponseWriter,
	r *http.Request,
	p httprouter.Params,
) {
	vendor := p.ByName("vendor")
	if lastFinish, err := cacheCrawlLastFinish(vendor); lastFinish != "" {
		fmt.Fprintf(w, "%s\n", lastFinish)
		return
	} else if err != nil {
		log.Error("Problem reading cache (%s): %v", vendor, err)
	}

	log.Trace("Cache miss (%s)", vendor)
	lastFinish, err := dbCrawlLastFinish(vendor)
	if err != nil {
		log.Error("Problem reading db: %v", err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}

	if err := fillCrawlLastFinish(vendor, lastFinish); err != nil {
		log.Error("Problem populating cache: %v", err)
	} else {
		log.Trace("Cache miss (%s) populated", vendor)
	}

	fmt.Fprintf(w, "%s\n", lastFinish)
}
